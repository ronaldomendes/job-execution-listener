# Spring Batch: Job Execution Listener

In this project you will learn how to add some additional visibility on 
job processing with JobExecutionListener using Spring Batch architecture.
