package com.cursospring.batch.jobexecutionlistener;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableBatchProcessing
@ComponentScan(basePackages = {"com.cursospring.batch.jobexecutionlistener"})
public class JobExecutionListenerApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobExecutionListenerApplication.class, args);
    }

}
